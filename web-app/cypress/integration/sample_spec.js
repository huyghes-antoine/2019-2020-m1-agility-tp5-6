describe('The Traffic web site home page', () => { 
    it('successfully loads', () => {
        cy.visit('/') 
    })
})

describe('Strory 1 -> The configuration page', () => {
    it('successfully loads', () =>{
        cy.get('a[href="#configuration"]').click() // Click on the configuration link in the navbar
        cy.url().should('include', '#configuration') // Check if the new URL is now the configuration page
    })
})

describe('Story 2 -> Should count number of segments', () => {
    it('successfully count segments', () => {
        cy.get('form[data-kind="segment"]').should('have.length', 28) // Check if the form "Segment" have 28 lines
    })
})

describe('Story 3 -> No car for the moment', () => {
    it('0 car', () => {
        cy.get('table').contains('No vehicle available') // Check if the table on the configuration page have really 0 vehicles inside
    })
})

describe('Story 4 -> Change speed of segment 5', () => {
    it('30 speed for segment 5', () => {

        cy.get('input[name="speed"][form="segment-5"]').clear().type('30') // Clear the speed for the segment 5 and retype the value 30 inside
        cy.get('form[id="segment-5"]').click() // Valide the change by clicking the update button
        cy.get('.modal-footer > .btn').click() // Close the modal by clicking the button

        cy.request('GET', 'http://127.0.0.1:4567/elements').as('elements') // Request to get our JSON page .../elements and put the 'elements' alias
        cy.get('@elements').should((response)=>{
            expect(response.body['segments']['4']).to.have.property('speed', 30) // We expect that the segment at the fourth places have 30 in speed property 
        })                                                                       // The fourth place = segment5 
    })
})

describe('Story 5 -> change roundabout parameters' , () => {
    it('Change roundabout to 4 and 15', () => {
        // Clear the roundabouts values and change them to 4 and 15
        cy.get(':nth-child(3) > .card-body > form > :nth-child(2) > .form-control').clear().type('4') 
        cy.get(':nth-child(3) > .card-body > form > :nth-child(3) > .form-control').clear().type('15')
        cy.get(':nth-child(3) > .card-body > form > .btn').click() // Valide by clicking the update button
        cy.get('.modal-footer > .btn').click() // close the modal

        cy.reload() // Refresh the page 

        cy.request('GET', 'http://127.0.0.1:4567/elements').as('elements')
        cy.get('@elements').should((response)=>{
            // Check that our crossroad values are equivalent in the JSON
            expect(response.body['crossroads']['2']).to.have.property('capacity',4) 
            expect(response.body['crossroads']['2']).to.have.property('duration', 15)
        })
    })
})

describe('Story 6 -> change traffic light 29', () => {
    it('Change to 4 / 40 / 8', () => {
        cy.get('a[href="#configuration"]').click()
        // Clear the traffic lights values and change them to 4 40 8
        cy.get(':nth-child(1) > .card-body > form > :nth-child(2) > .form-control').clear().type('4')
        cy.get(':nth-child(1) > .card-body > form > :nth-child(3) > .form-control').clear().type('40')
        cy.get(':nth-child(1) > .card-body > form > :nth-child(4) > .form-control').clear().type('8')
        cy.get(':nth-child(1) > .card-body > form > .btn').click() // Validate
        cy.get('.modal-footer > .btn').click() // Close the modal

        cy.request('GET', 'http://127.0.0.1:4567/elements').as('elements')
        cy.get('@elements').should((response)=>{
            // Check the values in our JSON 
            expect(response.body['crossroads']['0']).to.have.property('orangeDuration', 4)
            expect(response.body['crossroads']['0']).to.have.property('greenDuration', 40)
            expect(response.body['crossroads']['0']).to.have.property('nextPassageDuration', 8)
        })
    })
})

describe('Story 7 -> add vehicles from configuration', () =>{
    it('Add vehicles with different properties', () => {
        // Clear the values and change them to add 3 vehicles in our list
        cy.get('form > :nth-child(1) > .form-control').clear().type('5')
        cy.get('.col-md-4 > form > :nth-child(2) > .form-control').clear().type('26')
        cy.get('.col-md-4 > form > :nth-child(3) > .form-control').clear().type('50')
        cy.get('.col-md-4 > form > .btn').click()
        cy.get('.modal-footer > .btn').click()

        cy.get('form > :nth-child(1) > .form-control').clear().type('19')
        cy.get('.col-md-4 > form > :nth-child(2) > .form-control').clear().type('8')
        cy.get('.col-md-4 > form > :nth-child(3) > .form-control').clear().type('200')
        cy.get('.col-md-4 > form > .btn').click()
        cy.get('.modal-footer > .btn').click()

        cy.get('form > :nth-child(1) > .form-control').clear().type('27')
        cy.get('.col-md-4 > form > :nth-child(2) > .form-control').clear().type('2')
        cy.get('.col-md-4 > form > :nth-child(3) > .form-control').clear().type('150')
        cy.get('.col-md-4 > form > .btn').click()
        cy.get('.modal-footer > .btn').click()

        cy.request('GET', 'http://127.0.0.1:4567/vehicles').as('vehicles')
        cy.get('@vehicles').should((response)=>{
            // Check the if the speed is equal to 0 in the JSON
            expect(response.body['200.0']['0']).to.have.property('speed', 0)
            expect(response.body['150.0']['0']).to.have.property('speed', 0)
            expect(response.body['50.0']['0']).to.have.property('speed', 0)
        })
        // Check if the speed is equivalent in our configuration page 
        cy.get('.col-md-8 > .table > tbody > :nth-child(1) > :nth-child(3)').contains('0')
        cy.get('.col-md-8 > .table > tbody > :nth-child(2) > :nth-child(3)').contains('0')
        cy.get('.col-md-8 > .table > tbody > :nth-child(3) > :nth-child(3)').contains('0')
    })
})

describe('Story 8 -> all vehicles are stopped' , () => {
    it('Check the simulation', () => {
        cy.get(':nth-child(1) > .nav-link').click()  // Go to the simulation tab
        // Check if all our vehicles are stopped (so speed = 0)
        cy.get('.col-md-7 > .table > tbody > :nth-child(1) > :nth-child(3)').contains('0')
        cy.get('.col-md-7 > .table > tbody > :nth-child(2) > :nth-child(3)').contains('0')
        cy.get('.col-md-7 > .table > tbody > :nth-child(3) > :nth-child(3)').contains('0')
       
        cy.get('.form-control').clear().type('120') // Clear the simulation time and change it to 120
        cy.get('.btn').click() // Run the simulation 
        cy.get('.progress-bar', {timeout: 40000}).should('have.attr', 'aria-valuenow', '100') // After 40000 ms the progress bar should be full 
       
        // Check that 2 of our vehicles are stopped, and one is not
        cy.get('.col-md-7 > .table > tbody > :nth-child(1) > :nth-child(3)').contains('0')
        cy.get('.col-md-7 > .table > tbody > :nth-child(2) > :nth-child(3)').contains('0')
        cy.get('.col-md-7 > .table > tbody > :nth-child(3) > :nth-child(3)').contains('10')
    })
})

describe('Story 9 -> reload and remake simulation' , () => {
    it('Check the new simulation', () => {
        cy.reload()// Refresh the page to clean te JSON

        cy.request('GET', 'http://127.0.0.1:4567/vehicles').as('vehicles') // Request to get our localhost/vehicles route in the vehicles alias
        cy.get('@vehicles').should((response)=>{
            // Check if the JSON /vehicles is really empty
            expect(response.body).to.be.empty
        })

        cy.get('a[href="#configuration"]').click() // Go on the configuration tab
        // We add 3 vehicles with the same parameters than the story 7
        cy.get('form > :nth-child(1) > .form-control').clear().type('5')
        cy.get('.col-md-4 > form > :nth-child(2) > .form-control').clear().type('26')
        cy.get('.col-md-4 > form > :nth-child(3) > .form-control').clear().type('50')
        cy.get('.col-md-4 > form > .btn').click()
        cy.get('.modal-footer > .btn').click()
       
        cy.get('form > :nth-child(1) > .form-control').clear().type('19')
        cy.get('.col-md-4 > form > :nth-child(2) > .form-control').clear().type('8')
        cy.get('.col-md-4 > form > :nth-child(3) > .form-control').clear().type('200')
        cy.get('.col-md-4 > form > .btn').click()
        cy.get('.modal-footer > .btn').click()
       
        cy.get('form > :nth-child(1) > .form-control').clear().type('27')
        cy.get('.col-md-4 > form > :nth-child(2) > .form-control').clear().type('2')
        cy.get('.col-md-4 > form > :nth-child(3) > .form-control').clear().type('150')
        cy.get('.col-md-4 > form > .btn').click()
        
        cy.get('.modal-footer > .btn').click()
        // Go to the simulation tab to run a simulation of 500 secondes
        cy.get('a[href="#simulation"]').click()
       
        cy.get('.form-control').clear().type('500')
        cy.get('.btn').click()
       
        cy.get('.progress-bar',{timeout: 155000}).should('have.attr', 'aria-valuenow', '100') // Check if the progress bar is full after the timeout
        // Check if our 3 vehicles are stopped
        cy.get('.col-md-7 > .table > tbody > :nth-child(1) > :nth-child(3)').contains('0')
        cy.get('.col-md-7 > .table > tbody > :nth-child(2) > :nth-child(3)').contains('0')
        cy.get('.col-md-7 > .table > tbody > :nth-child(3) > :nth-child(3)').contains('0')
    })
})

describe('Story 10 -> reload and remake simulation' , () => {
    it('Check the new simulation and cars position', () => {
        cy.reload() // Refresh the page to clean the JSON

        cy.request('GET', 'http://127.0.0.1:4567/vehicles').as('vehicles')
        cy.get('@vehicles').should((response)=>{
            // Check if our JSON /vehicles is really empty 
            expect(response.body).to.be.empty
        })

        // Add 3 new vehicles with different parameters
        cy.get('a[href="#configuration"]').click()
        cy.get('form > :nth-child(1) > .form-control').clear().type('5')
        cy.get('.col-md-4 > form > :nth-child(2) > .form-control').clear().type('26')
        cy.get('.col-md-4 > form > :nth-child(3) > .form-control').clear().type('50')
        cy.get('.col-md-4 > form > .btn').click()
        cy.get('.modal-footer > .btn').click()
        
        cy.get('form > :nth-child(1) > .form-control').clear().type('5')
        cy.get('.col-md-4 > form > :nth-child(2) > .form-control').clear().type('26')
        cy.get('.col-md-4 > form > :nth-child(3) > .form-control').clear().type('80')
        cy.get('.col-md-4 > form > .btn').click()
        cy.get('.modal-footer > .btn').click()
        
        cy.get('form > :nth-child(1) > .form-control').clear().type('5')
        cy.get('.col-md-4 > form > :nth-child(2) > .form-control').clear().type('26')
        cy.get('.col-md-4 > form > :nth-child(3) > .form-control').clear().type('80')
        cy.get('.col-md-4 > form > .btn').click()
        cy.get('.modal-footer > .btn').click()
        
        // Go to the simulation tab and run a simulation for 200 seconds
        cy.get('a[href="#simulation"]').click()
        
        cy.get('.form-control').clear().type('200')
        cy.get('.btn').click()
        cy.get('.progress-bar', {timeout: 60000}).should('have.attr', 'aria-valuenow', '100') // Check if the progress bar is full after the timeout
       
        // Check the position of our vehicles 
        cy.get('tbody > :nth-child(1) > :nth-child(5)').contains('29')
        cy.get('tbody > :nth-child(2) > :nth-child(5)').contains('29')
        cy.get('tbody > :nth-child(3) > :nth-child(5)').contains('17')
    })
})


/* ------- TESTS WITH NEW DOM ID's ------- */

describe('REAMKE -> The Traffic web site home page', () => { 
    it('successfully loads', () => {
        cy.visit('/') 
    })
})

describe('REMAKE Strory 1 -> The configuration page', () => {
    it('successfully loads', () =>{
        cy.get('a[href="#configuration"]').click() 
        cy.url().should('include', '#configuration') 
    })
})

describe('REMAKE Story 2 -> Should count number of segments', () => {
    it('successfully count segments', () => {
        cy.get('form[data-kind="segment"]').should('have.length', 28) 
    })
})

describe('REMAKE Story 3 -> No car for the moment', () => {
    it('0 car', () => {
        cy.get('table[id="VehiclesList"]').contains('No vehicle available') // add id to this table
    })
})

describe('REMAKE Story 4 -> Change speed of segment 5', () => {
    it('30 speed for segment 5', () => {
        cy.get('input[name="speed"][form="segment-5"]').clear().type('30') 
        cy.get('form[id="segment-5"]').click() 
        cy.get('button[id="closeModal').click() // Add id to close the modal

        cy.request('GET', 'http://127.0.0.1:4567/elements').as('elements') 
        cy.get('@elements').should((response)=>{
            expect(response.body['segments']['4']).to.have.property('speed', 30) 
        })                                                                       
    })
})

describe('REMAKE Story 5 -> change roundabout parameters' , () => {
    it('Change roundabout to 4 and 15', () => {
        cy.get('input[id="capacityInput"]').clear().type('4') // Add id to the capacity input
        cy.get('input[id="durationInput"]').clear().type('15') // Add id to the duration input
        cy.get('button[id="updateRoundabout"]').click()  // Add id to the update button of the roundabout
        cy.get('button[id="closeModal"]').click() 
        cy.reload()

        cy.request('GET', 'http://127.0.0.1:4567/elements').as('elements')
        cy.get('@elements').should((response)=>{
            expect(response.body['crossroads']['2']).to.have.property('capacity',4) 
            expect(response.body['crossroads']['2']).to.have.property('duration', 15)
        })
    })
})

describe('REMAKE Story 6 -> change traffic light 29', () => {
    it('Change to 4 / 40 / 8', () => {
        cy.get('a[href="#configuration"]').click()
        cy.get('input[id="orangeInput"][form="trafficlight-29"]').clear().type('4') // Add id to orange duration input and take the one with id 29
        cy.get('input[id="greenInput"][form="trafficlight-29"]').clear().type('40') // Add id to green duration input and take the one with id 29
        cy.get('input[id="nextPassageInput"][form="trafficlight-29"]').clear().type('8') // Add id to next passage input and take the one with id 29
        cy.get('button[id="updateLights29"]').click() // Add id to the update button with id parameter
        cy.get('button[id="closeModal"]').click() 

        cy.request('GET', 'http://127.0.0.1:4567/elements').as('elements')
        cy.get('@elements').should((response)=>{
            expect(response.body['crossroads']['0']).to.have.property('orangeDuration', 4)
            expect(response.body['crossroads']['0']).to.have.property('greenDuration', 40)
            expect(response.body['crossroads']['0']).to.have.property('nextPassageDuration', 8)
        })
    })
})

describe('REMAKE Story 7 -> add vehicles from configuration', () =>{
    it('Add vehicles with different properties', () => {

        cy.get('input[id="originInput"]').clear().type('5') // Add id to the origin input for vehicles
        cy.get('input[id="destinationInput"]').clear().type('26') // Add id to destination input for vehicles
        cy.get('input[id="timeInput"]').clear().type('50') // Add id to time input for vehicles
        cy.get('button[id="addVehicle"]').click() // Add id to the button to add vehicle
        cy.get('button[id="closeModal"]').click()

        cy.get('input[id="originInput"]').clear().type('19')
        cy.get('input[id="destinationInput"]').clear().type('8')
        cy.get('input[id="timeInput"]').clear().type('200')
        cy.get('button[id="addVehicle"]').click()
        cy.get('button[id="closeModal"]').click()

        cy.get('input[id="originInput"]').clear().type('27')
        cy.get('input[id="destinationInput"]').clear().type('2')
        cy.get('input[id="timeInput"]').clear().type('150')
        cy.get('button[id="addVehicle"]').click()
        cy.get('button[id="closeModal"]').click()

        cy.request('GET', 'http://127.0.0.1:4567/vehicles').as('vehicles')
        cy.get('@vehicles').should((response)=>{
            expect(response.body['200.0']['0']).to.have.property('speed', 0)
            expect(response.body['150.0']['0']).to.have.property('speed', 0)
            expect(response.body['50.0']['0']).to.have.property('speed', 0)
        })

        cy.get('th[id="from200.0"]').contains('0') // Add id to check the speed of the vehicle from the time of the vehicle (because we don't have a lot of examples)
        cy.get('th[id="from50.0"]').contains('0')
        cy.get('th[id="from150.0"]').contains('0')
    })
})

describe('REMAKE Story 8 -> all vehicles are stopped' , () => {
    it('Check the simulation', () => {
        
        cy.get('a[href="#simulation"]').click()

        cy.get('th[id="fromsimu200.0"]').contains('0') 
        cy.get('th[id="fromsimu50.0"]').contains('0')
        cy.get('th[id="fromsimu150.0"]').contains('0')

        cy.get('input[id="runValue"]').clear().type('120')  // Add id to the run input 
        cy.get('button[id="runSimulation"]').click()  // Add id to the button to run the simulation

        cy.get('.progress-bar', {timeout: 40000}).should('have.attr', 'aria-valuenow', '100') 

        cy.get('th[id="fromsimu200.0"]').contains('0') 
        cy.get('th[id="fromsimu50.0"]').contains('10')
        cy.get('th[id="fromsimu150.0"]').contains('0')
        })
})

describe('REMAKE Story 9 -> reload and remake simulation' , () => {
    it('Check the new simulation', () => {
        cy.reload()/

        cy.request('GET', 'http://127.0.0.1:4567/vehicles').as('vehicles') // Request to get our localhost/vehicles route in the vehicles alias
        cy.get('@vehicles').should((response)=>{
            expect(response.body).to.be.empty
        })

        cy.get('a[href="#configuration"]').click()

        cy.get('input[id="originInput"]').clear().type('5') 
        cy.get('input[id="destinationInput"]').clear().type('26') 
        cy.get('input[id="timeInput"]').clear().type('50') 
        cy.get('button[id="addVehicle"]').click() 
        cy.get('button[id="closeModal"]').click()

        cy.get('input[id="originInput"]').clear().type('19')
        cy.get('input[id="destinationInput"]').clear().type('8')
        cy.get('input[id="timeInput"]').clear().type('200')
        cy.get('button[id="addVehicle"]').click()
        cy.get('button[id="closeModal"]').click()

        cy.get('input[id="originInput"]').clear().type('27')
        cy.get('input[id="destinationInput"]').clear().type('2')
        cy.get('input[id="timeInput"]').clear().type('150')
        cy.get('button[id="addVehicle"]').click()
        cy.get('button[id="closeModal"]').click()

        cy.get('a[href="#simulation"]').click()

        cy.get('input[id="runValue"]').clear().type('500')
        cy.get('button[id="runSimulation"]').click()
        cy.get('.progress-bar',{timeout: 155000}).should('have.attr', 'aria-valuenow', '100') 

        cy.get('th[id="fromsimu200.0"]').contains('0') 
        cy.get('th[id="fromsimu50.0"]').contains('0')
        cy.get('th[id="fromsimu150.0"]').contains('0')
    })
})

describe('REMAKE Story 10 -> reload and remake simulation' , () => {
    it('Check the new simulation and cars position', () => {
        cy.reload() 

        cy.request('GET', 'http://127.0.0.1:4567/vehicles').as('vehicles')
        cy.get('@vehicles').should((response)=>{
            expect(response.body).to.be.empty
        })

        cy.get('a[href="#configuration"]').click()

        cy.get('input[id="originInput"]').clear().type('5') 
        cy.get('input[id="destinationInput"]').clear().type('26') 
        cy.get('input[id="timeInput"]').clear().type('50') 
        cy.get('button[id="addVehicle"]').click() 
        cy.get('button[id="closeModal"]').click()

        cy.get('input[id="originInput"]').clear().type('5')
        cy.get('input[id="destinationInput"]').clear().type('26')
        cy.get('input[id="timeInput"]').clear().type('80')
        cy.get('button[id="addVehicle"]').click()
        cy.get('button[id="closeModal"]').click()

        cy.get('input[id="originInput"]').clear().type('5')
        cy.get('input[id="destinationInput"]').clear().type('26')
        cy.get('input[id="timeInput"]').clear().type('80')
        cy.get('button[id="addVehicle"]').click()
        cy.get('button[id="closeModal"]').click()

        cy.get('a[href="#simulation"]').click()
        cy.get('input[id="runValue"]').clear().type('200')
        cy.get('button[id="runSimulation"]').click()

        cy.get('.progress-bar', {timeout: 60000}).should('have.attr', 'aria-valuenow', '100') 

        cy.get('th[id="poseSimu80.0"]').contains('29') // Add id for the position ofthe vehicles
        cy.get('th[id="poseSimu80.0"]').contains('29')
        cy.get('th[id="poseSimu50.0"]').contains('17')
    })
})